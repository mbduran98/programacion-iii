﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SegundoParcial
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnLeer_Click(object sender, EventArgs e)
        {
            string line;
            string ruta = @"C:\Users\Usuario\source\repos\programacion-iii\SegundoParcial\segundo_examen_parcial_parametros.txt";
            List<string> reg = new List<string>();
            try
            {
                string busqueda = "install.port=";
                foreach(string item in File.ReadAllLines(ruta,Encoding.Default))
                {
                    if(item.Contains(busqueda))
                    {
                        reg.Add(item);
                    }
                }
                foreach(string item in reg)
                {
                    txtPort.AppendText(item);
                }
                reg.Clear();

            }
            catch (Exception)
            {
                MessageBox.Show("Error de lectura");
            }

            try
            {
                string busqueda = "install.option=";
                foreach (string item in File.ReadAllLines(ruta, Encoding.Default))
                {
                    if (item.Contains(busqueda))
                    {
                        reg.Add(item);
                    }
                }
                foreach (string item in reg)
                {
                    
                }
                reg.Clear();

            }
            catch (Exception)
            {
                MessageBox.Show("Error de lectura");
            }
            try
            {
                string busqueda = "install.address=";
                foreach (string item in File.ReadAllLines(ruta, Encoding.Default))
                {
                    if (item.Contains(busqueda))
                    {
                        reg.Add(item);
                    }
                }
                foreach (string item in reg)
                {
                    txtIP.AppendText(item);
                }
                reg.Clear();

            }
            catch (Exception)
            {
                MessageBox.Show("Error de lectura");
            }
            try
            {
                string busqueda = "install.password=";
                foreach (string item in File.ReadAllLines(ruta, Encoding.Default))
                {
                    if (item.Contains(busqueda))
                    {
                        reg.Add(item);
                    }
                }
                foreach (string item in reg)
                {
                    txtPassword.AppendText(item);
                }
                reg.Clear();

            }
            catch (Exception)
            {
                MessageBox.Show("Error de lectura");
            }
            try
            {
                string busqueda = "LOCALIDADINVENTARIO=";
                foreach (string item in File.ReadAllLines(ruta, Encoding.Default))
                {
                    if (item.Contains(busqueda))
                    {
                        reg.Add(item);
                    }
                }
                foreach (string item in reg)
                {
                    
                }

                reg.Clear();
            }
            catch (Exception)
            {
                MessageBox.Show("Error de lectura");
            }
        }

        private void txtPort_TextChanged(object sender, EventArgs e)
        {
        }
    }
}