﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormaCaptura : Form
    {
        string[,] personas = new string[,]
        { { "Angel","Christopher","Daniel","Daniel","Edgar","Fernando","Francisco","Ivan","Joel","Juan","Kevin","Luis","Manuel","Mario","Marisol","Mauricio","Oscar","Yahayra","Cesar" }
    };
        string[,] apellidos = new string[,]{{ "Duran" , "Villalobos", "Lopez", "Vazquez", "Banuelos","Hernandez","Garcia","Narvaez","Juarez","Romero","Gonzalez","Gomez","Mariscal","Mercado","Benitez","Castaneda",
        "Ochoa","Rodriguez","De la Cruz" } };


        public FormaCaptura()
        {
            InitializeComponent();
        }
        
    private void btnBuscar_Click(object sender, EventArgs e)
        {
            string ap = Convert.ToString(apellidos);
            string n = Convert.ToString(personas);
            string a = Convert.ToString(txtBuscaNombre.Text);
            string b = Convert.ToString(txtBuscaApellido.Text);
            if ((a == n) && (b == ap))
            {
                MessageBox.Show(txtBuscaNombre + " " + txtBuscaApellido, "Encabezado");
            }
                    
            else  
            {
                MessageBox.Show("El nombre no se encuentra");
            }

            
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtBuscaApellido.Text = " ";
            txtBuscaNombre.Text = " ";
        }
    }
}
