﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(txtValor1.Text);
            double b = Convert.ToDouble(txtValor2.Text);
            double c = a + b;
            lblResultadoOperacion.Text = c.ToString();

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            lblResultadoOperacion.Text = "0";
            txtValor1.Text = "";
            txtValor2.Text = "";
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(txtValor1.Text);
            double b = Convert.ToDouble(txtValor2.Text);
            double c = a - b;
            lblResultadoOperacion.Text = c.ToString();
        }

        private void btnMult_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(txtValor1.Text);
            double b = Convert.ToDouble(txtValor2.Text);
            double c = a * b;
            lblResultadoOperacion.Text = c.ToString();
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(txtValor1.Text);
            double b = Convert.ToDouble(txtValor2.Text);
            if (b == 0)
            {
                MessageBox.Show("No es posible dividir entre 0, coloque otro número");
                lblResultadoOperacion.Text = "";
            }
            else
            {
                double c = a / b;
                lblResultadoOperacion.Text = c.ToString();
            }
        }
    }
}
