﻿using System;

namespace MensajeOculto
{
    class Program
    {
        static void Main(string[] args)
        {

            String[] S1 = { "c", "d", "e", "o" };
            String[] S2 = { "c", "d", "e", "e", "n", "e", "t", "p", "i" };
            String[] S3 = { "b", "y", "t", "d", "a", "g" };
            int[] A1 = { 3, 2, 0, 1 };
            int[] A2 = { 5, 2, 0, 1, 6, 4, 8, 3, 7 };
            int[] A3 = { 4, 3, 0, 1, 2, 5 };

                Console.WriteLine(S1[A1[2]] + S1[A1[0]] + S1[A1[3]] + S1[A1[1]]);
                Console.WriteLine(S2[A2[2]] + S2[A2[0]] + S2[A2[5]] + S2[A2[4]]+S2[A2[6]]+S2[A2[8]]+S2[A2[7]]+S2[A2[3]]+S2[A2[1]]);
                Console.WriteLine(S3[A3[2]] + S3[A3[0]] + S3[A3[4]]);
        }
    }
}